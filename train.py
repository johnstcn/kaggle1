#!/usr/bin/env python3

import argparse
import csv
import datetime
import pickle

import numpy as np
import pandas as pd

from scipy import stats

from sklearn import linear_model
from sklearn.impute import SimpleImputer
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import KFold, train_test_split
from sklearn.preprocessing import OneHotEncoder


NOW = datetime.datetime.now()
COUNTRIES = set()

'''
preprocess reads the data contained in fpath and returns a cleaned dataframe
'''
def preprocess(fpath):
    raw = []
    rows = []
    with open(fpath) as f:
        reader = csv.DictReader(f)
        raw = [r for r in reader]

    # XXX: global variables are bad
    global COUNTRIES
    COUNTRIES = set(map(lambda d : d['Country'].lower(), raw))

    for row in raw:
        d = {
            'age': handle_float(row['Age']),
            'income': handle_float(row['Income in EUR']),
            'year': handle_float(row['Year of Record']),
            'size': handle_float(row['Size of City']),
            'gender_male': onehot(row['Gender'], 'male'),
            'gender_female': onehot(row['Gender'], 'female'),
            'gender_other': onehot(row['Gender'], 'other'),
            'edu_b': onehot(row['University Degree'], 'bachelor'),
            'edu_m': onehot(row['University Degree'], 'master'),
            'edu_p': onehot(row['University Degree'], 'phd'),
        }
        for country in COUNTRIES:
            idx = country.replace(' ', '_')
            d['country_' + country] = onehot(row['Country'], country)
        rows.append(d)
    df = pd.DataFrame(rows)
    df.fillna(df.mean(), inplace=True)
    return df

# handlers for various data types
def handle_float(s):
    try:
        return float(s)
    except:
        return np.nan


def handle_year(s):
    try:
        y = int(s)
        if y > NOW.year:
            return NOW.year
        if y < 1970: # arbitrary
            return 1970
    except:
        return NOW.year


def onehot(s, expected):
    if s.lower().strip() == expected:
        return 1.0
    return 0.0


def main():
    parser = argparse.ArgumentParser(description='train a model to predict income')
    parser.add_argument('training_data', help='path to input training data')
    parser.add_argument('model_name', help='path to save output model')
    parser.add_argument('--verbose', '-v', help='print additional data to stdout', action='store_true')
    args = parser.parse_args()

    preprocessed = preprocess(args.training_data)

    if args.verbose:
        print('### Sample of preprocessed data ###')
        print(preprocessed.sample(10))
        print('\n### Description of preprocessed data ###')
        print(preprocessed.describe(include='all'))
   

    features = [
        'age',
        'year',
        'size',
        'gender_male',
        'gender_female',
        'gender_other',
        'edu_b',
        'edu_m',
        'edu_p',
    ]
    # XXX: this is terrible but country is such a juicy predictor
    features.extend(['country_' + c for c in COUNTRIES])

    xs = preprocessed[features]
    ys = preprocessed['income']

    m = linear_model.LinearRegression()
    # XXX: monkey-patching is something ruby people do
    setattr(m, 'known_features', features)

    # do some cross-validation
    cv = KFold(n_splits=10, shuffle=True)
    xs_train, xs_test, ys_train, ys_test = train_test_split(xs, ys, test_size=0.8)

    m.fit(xs_train, ys_train)
    ys_pred = m.predict(xs_test)
    if args.verbose:
        score = m.score(xs_test, ys_test)
        print('R^2 score: {0:2f}'.format(score))
        rmse = np.sqrt(mean_squared_error(ys_test, ys_pred))
        print('RMSE score: {0:2f}'.format(rmse))

    ys_pred = m.predict(xs)

    with open(args.model_name, 'wb') as f:
        pickle.dump(m, f)


if __name__ == "__main__":
    main()
