#!/usr/bin/env python3

import argparse
import csv
import datetime
import pickle

import numpy as np
import pandas as pd

from scipy import stats

from sklearn import linear_model
from sklearn.impute import SimpleImputer
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import OneHotEncoder

NOW = datetime.datetime.now()

'''
preprocess reads the data contained in fpath and returns a cleaned dataframe
'''
def preprocess(fpath, known_features):
    raw = []
    rows = []
    with open(fpath) as f:
        reader = csv.DictReader(f)
        raw = [r for r in reader]

    # XXX: global variables are bad
    countries = set()
    for f in known_features:
        if f.startswith('country_'):
            countries.add(f.replace('country_', ''))

    for row in raw:
        d = {
            'instance': handle_int(row['\ufeffInstance']),
            'age': handle_float(row['Age']),
            'year': handle_float(row['Year of Record']),
            'size': handle_float(row['Size of City']),
            'gender_male': onehot(row['Gender'], 'male'),
            'gender_female': onehot(row['Gender'], 'female'),
            'gender_other': onehot(row['Gender'], 'other'),
            'edu_b': onehot(row['University Degree'], 'bachelor'),
            'edu_m': onehot(row['University Degree'], 'master'),
            'edu_p': onehot(row['University Degree'], 'phd'),
        }
        for country in countries:
            idx = country.replace(' ', '_')
            d['country_' + country] = onehot(row['Country'], country)
        rows.append(d)
    df = pd.DataFrame(rows)
    df.fillna(df.mean(), inplace=True)
    return df

# handlers for various data types
def handle_int(s):
    try:
        return int(s)
    except:
        return np.nan

def handle_float(s):
    try:
        return float(s)
    except:
        return np.nan


def handle_year(s):
    try:
        y = int(s)
        if y > NOW.year:
            return NOW.year
        if y < 1970: # arbitrary
            return 1970
    except:
        return NOW.year


def onehot(s, expected):
    if s.lower().strip() == expected:
        return 1.0
    return 0.0


def main():
    parser = argparse.ArgumentParser(description='predict income')
    parser.add_argument('model', help='path to model model')
    parser.add_argument('input', help='path to input data')
    parser.add_argument('output', help='path to output')
    parser.add_argument('--verbose', '-v', help='print additional data to stdout', action='store_true')
    args = parser.parse_args()

    m = None
    with open(args.model, 'rb') as f:
        m = pickle.load(f)

    preprocessed = preprocess(args.input, m.known_features)

    if args.verbose:
        print('### Sample of preprocessed input data ###')
        print(preprocessed.sample(10))
        print('\n### Description of preprocessed input data ###')
        print(preprocessed.describe(include='all'))
    
    xs = preprocessed[m.known_features]

    with open(args.output, 'w') as f:
        f.write('Instance,Income\n')
        for (instance_id, (_, row)) in zip(preprocessed['instance'],  xs.iterrows()):
            predicted = m.predict([row])
            f.write('{0},{1:.2f}\n'.format(instance_id ,predicted[0]))


if __name__ == "__main__":
    main()
