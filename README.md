# Income Prediction

This repository contains Python code to train a model to predict peoples' income.

## Setup

You'll need Python3 and Pipenv. One you have those installed on your platform, just run `pipenv install` and all the required dependencies will be fetched as specified in `Pipfile`/`Pipfile.lock`.

## Training 

`train.py` trains a model from a given input and persists it to a file. Add the `--verbose` flag to print some possibly helpful data to `stdout`.

Usage: 
```
pipenv run ./train.py path/to/training/data.csv path/to/output.csv
```

## Predicting

`predict.py` predicts data given a previously trained model and an input file. Add the `--verbose` flag to print some possibly helpful data to `stdout`.

Usage:
```
pipenv run ./predict.py path/to/model.pickle path/to/input.csv path/to/output.csv
```

## Other Approaches

This branch contains the best-peforming code only.
Other less performant approaches can be seen in different branches:
 * `v0`: an extension of the code in this branch that attempted using profession as a feature through a combination of stopword removal and tokenisation
 * `v1`: a ground-up rewrite attempting to replace hand-rolled code with `scikit-learn` native functions

